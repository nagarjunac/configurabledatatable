public with sharing class DataTableController {
    @AuraEnabled(cacheable=true)
    public static List<CofigurableDataTable__mdt> getColumnConfiguration(Integer pageNumber, Integer pageSize) {
        
        return [
            SELECT Conditions__c, Configuration__c, Is_Active__c, Object_API_Name__c, Table_Title__c
            FROM CofigurableDataTable__mdt
          
        ];
    }
    
    @AuraEnabled(cacheable=true)
    public static List<SObject> fetchData(string objectName, List<string> fields, Integer pageNumber, Integer pageSize, String sortByFieldName, String sortDirection) {
       
        String fieldsList = String.join(fields, ', ');
        String orderBy = sortByFieldName != null ? 'ORDER BY ' + sortByFieldName + ' ' + sortDirection : '';
        String qry = 'SELECT ' + fieldsList + ' FROM ' + objectName + ' ' + orderBy + ' LIMIT ' + pageSize + ' OFFSET ' + pageNumber;
        System.debug('qry: ' + qry);
        
        List<SObject> records = database.query(qry);
        return records;
    }
    
    @AuraEnabled
    public static void saveData(List<SObject> updatedRecords) {
       
        update updatedRecords;
    }
}