import { LightningElement, wire , track} from 'lwc';
import getColumnConfiguration from '@salesforce/apex/DataTableController.getColumnConfiguration';
import fetchData from '@salesforce/apex/DataTableController.fetchData';
import saveData from '@salesforce/apex/DataTableController.saveData';

export default class DataTableControllerLwc extends LightningElement {

    @track columns = [];
    @track data = [];
    @track draftValues = [];
    @track objectApiName;
    @track tableTitle;
    @track pageNumber = 0;
    @track pageSize = 5;
    @track sortByFieldName;
    @track sortDirection = 'ASC';
    @track loadMore = true;
    @track fields = [];
    @track firstRender = false ;

    @wire(getColumnConfiguration, { pageNumber: '$pageNumber', pageSize: '$pageSize' })
    retrieveColumnConfig({ data, error }) {
        if (data) {
            console.log('data', data);
            this.tableTitle = data[0].Table_Title__c;

            this.objectApiName = data[0].Object_API_Name__c;
            let configurations = JSON.parse(data[0].Configuration__c);
            console.log('configurations', configurations, configurations.length);
            let localColumns = [];
            let fields = [];
            for (let i = 0; i < configurations.length; i++) {
                let columnVal = {
                    label: configurations[i].label,
                    fieldName: configurations[i].Name,
                    type: configurations[i].type,
                    editable: true 

                };
                localColumns.push(columnVal);
                fields.push(configurations[i].Name);
               
            }
            console.log('localColumns', localColumns);
            this.fields = fields;
            this.columns = localColumns;
            if (this.columns ) {
                if(this.objectApiName !== null && this.fields.length !== 0 ){
                    
                    
                    if(this.firstRender === false){
                        console.log('this.firstRender ========>',this.firstRender);
                        this.getData(this.pageNumber, this.pageSize);
                       } 
                
                }
            
            }
        } else if (error) {
            console.error(error);
        }
    }

    getData(pageNumber, pageSize) {
        console.log('fetchData ======>');
      return   fetchData({
            objectName: this.objectApiName,
            fields: this.fields,
            pageNumber: pageNumber,
            pageSize: pageSize,
            sortByFieldName: this.sortByFieldName,
            sortDirection: this.sortDirection
        })
            .then(result => {
                 console.log('result======>', result);
                
                let updatedRecords = [...this.data, ...result];
                this.data = updatedRecords;
                this.firstRender = true;
                if (result.length == 0) {
                    this.loadMore = false;
                }
            })
            .catch(error => {
                console.error(error);                
                // console.log('error =====>',error);
                this.loadMore = false;
            });
    }

    handleSave(event) {
        const updatedRecords = event.detail.draftValues;
        if (updatedRecords.length > 0) {
            saveData({ updatedRecords })
                .then(() => {
                    // Handle any additional logic after successful save
                    this.draftValues = [];
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }

    handleSort(event) {
        const { fieldName, sortDirection } = event.detail;
        this.sortByFieldName = fieldName;
        this.sortDirection = sortDirection;
        this.pageNumber = 1; // Reset page number when sorting is applied
        this.getData(this.columns.map(column => column.fieldName), this.objectApiName);
    }

   

    get showPrevious() {
        return this.pageNumber > 1;
    }

    get showNext() {
        return this.data && this.data.length === this.pageSize;
    }
    loadData(){

        return  this.getData(this.pageNumber, this.pageSize);


        
        // console.log('loadData ======>',loadData);
    }
    loadMoreData(event) {
        // if(this.loadMore){
            const currentRecord = this.accounts;
            const { target } = event;
            target.isLoading = true;
            
    
            this.pageNumber = this.pageNumber + this.pageSize;
            // console.log('this.pageNumber===========>',this.pageNumber);
            this.getData(this.pageNumber, this.pageSize)
           
                .then(()=> {
                    target.isLoading = false;
                    // console.log('target.isLoading ============+++++++++===>',target.isLoading);
                });   
        // }
       
    }
}
